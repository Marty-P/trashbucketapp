package m00n1i9ht.trashbucketapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_ARCHIVE;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_AUDIO;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_BIN;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_DOC;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_PIC;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_VIDEO;

/**
 * Created by Marty on 19.05.2017.
 */

public class FileDataRecyclerAdapter extends RecyclerView.Adapter<FileDataRecyclerAdapter.ViewHolder> {

    ArrayList<FileData> fileData;
    private OnClick onClick;

    public interface OnClick{
        void onItemClicked(int position, ViewHolder holder,  ArrayList<FileData> fileData);
        void onLongItemClicked(int position, ViewHolder holder,  ArrayList<FileData> fileData);
    };

    public FileDataRecyclerAdapter(ArrayList<FileData> data, OnClick _onClick){
        fileData = data;
        onClick = _onClick;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView_fileName;
        public TextView textView_size;
        public TextView textView_time;
        public ImageView imageView_type;
        public ImageView imageView_downloaded;
        public ProgressBar progressBar;
        //types

        public Drawable id_ico_pic;
        public Drawable id_ico_video;
        public Drawable id_ico_arch;
        public Drawable id_ico_bin;
        public Drawable id_ico_doc;
        public Drawable id_ico_audio;
        public Drawable id_ico_unknown;


        public ViewHolder(View itemView) {
            super(itemView);
            textView_fileName = (TextView)itemView.findViewById(R.id.textView_fileName);
            textView_size = (TextView)itemView.findViewById(R.id.textView_size);
            textView_time = (TextView)itemView.findViewById(R.id.textView_time);
            imageView_type = (ImageView)itemView.findViewById(R.id.imageView_type);
            imageView_downloaded = (ImageView)itemView.findViewById(R.id.imageView_downloaded);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);


            //types
            id_ico_pic = itemView.getResources().getDrawable(R.drawable.ico_pic);
            id_ico_video = itemView.getResources().getDrawable(R.drawable.ico_video);
            id_ico_arch = itemView.getResources().getDrawable(R.drawable.ico_arch);
            id_ico_bin = itemView.getResources().getDrawable(R.drawable.ico_bin1);
            id_ico_doc = itemView.getResources().getDrawable(R.drawable.ico_doc);
            id_ico_audio = itemView.getResources().getDrawable(R.drawable.ico_audio);
            id_ico_unknown = itemView.getResources().getDrawable(R.drawable.ico_unknow);
        }
    }


    @Override
    public FileDataRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_list, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textView_fileName.setText(fileData.get(position).getFileName());
        holder.textView_time.setText(fileData.get(position).getUploadTime());
        holder.textView_size.setText(fileData.get(position).getFullSize());
        String titleType = fileData.get(position).getTitleType();
        if(titleType.equals(TYPE_PIC))
            holder.imageView_type.setImageDrawable(holder.id_ico_pic);
        else if(titleType.equals(TYPE_DOC))
            holder.imageView_type.setImageDrawable(holder.id_ico_doc);
        else if(titleType.equals(TYPE_VIDEO))
            holder.imageView_type.setImageDrawable(holder.id_ico_video);
        else if(titleType.equals(TYPE_AUDIO))
            holder.imageView_type.setImageDrawable(holder.id_ico_audio);
        else if(titleType.equals(TYPE_BIN))
            holder.imageView_type.setImageDrawable(holder.id_ico_bin);
        else if(titleType.equals(TYPE_ARCHIVE))
            holder.imageView_type.setImageDrawable(holder.id_ico_arch);
        else
            holder.imageView_type.setImageDrawable(holder.id_ico_unknown);

        if(fileData.get(position).isDownloded())
            holder.imageView_downloaded.setVisibility(View.VISIBLE);
        else
            holder.imageView_downloaded.setVisibility(View.INVISIBLE);


        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onClick.onItemClicked(position, holder, fileData);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onClick.onLongItemClicked(position, holder, fileData);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return fileData.size();
    }
}
