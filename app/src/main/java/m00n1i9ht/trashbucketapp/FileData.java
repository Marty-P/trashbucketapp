package m00n1i9ht.trashbucketapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_ARCHIVE;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_AUDIO;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_BIN;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_DOC;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_PIC;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_UNKNOWN;
import static m00n1i9ht.trashbucketapp.GlobalThing.TYPE_VIDEO;

/**
 * Created by Marty on 19.05.2017.
 */

public class FileData {
    private String fileName;
    private String hash;
    private String uploadTime;
    private String idFile;
    private String type;
    private String titleType;
    private double sizeByte;
    private double sizeShortView;
    private String dimension;
    private boolean downloaded;
    private Context context;

    FileData(String _fileName, String _hash, String _uploadTime,
             String _idFile, String _type, String _size, Context _context){
        context = _context;
        fileName = _fileName;
        hash = _hash;
        uploadTime = contructData(_uploadTime);



        idFile = _idFile;
        type = _type;
        titleType = getTitleType(_type);
        try {
            sizeByte = Double.parseDouble(_size);
        } catch (NumberFormatException e) {
            sizeByte = 0;
        }

        String[] units = {"b", "KiB", "MiB", "GiB", "TiB"};
        int i = 0;
        sizeShortView = sizeByte;


        while(sizeShortView > 1023) {
            sizeShortView /= 1024;
            i++;
        }
        sizeShortView = Math.ceil(sizeShortView*100)/100;
        dimension = units[i];
        downloaded = checkDownloaded();
    }

    private String contructData(String _uploadTime){
        Double d_time = Double.parseDouble(_uploadTime);
        Integer i_time = d_time.intValue();
        Date date = new Date(i_time*1000L);
        SimpleDateFormat simpleDateFormatUpload = new SimpleDateFormat("HH:mm dd.MM.yy");
        simpleDateFormatUpload.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
        uploadTime = simpleDateFormatUpload.format(date);

        return uploadTime;
    }

    private boolean checkDownloaded(){
        if(context == null)
            return false;
        else{
            DataBase dataBase = new DataBase(context);
            SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
            Cursor cursor = sqLiteDatabase.query(DataBase.DATABASE_TABLE_FILES,null,
                    DataBase.KEY_HASH + " = ? AND " + DataBase.KEY_IDFILE + " = ?",
                    new String[]{hash, idFile}, null, null, null);
            if(cursor.getCount() != 0)
                return true;
            else
                return false;
        }
    }

    public String getFileName(){
        return fileName;
    }

    public String getUploadTime(){
        return uploadTime;
    }

    public String getType(){
        return type;
    }

    public String getFullSize(){
        return Double.toString(sizeShortView) + " " + dimension;
    }

    public String getHash(){
        return hash;
    }

    public String getIdFile(){
        return idFile;
    }

    public String getSize(){
        return Double.toString(sizeByte);
    }

    public String getTitleType(){ return titleType; };

    public boolean isDownloded(){ return downloaded; };

    private String getTitleType(String type){
        String[] TYPES_PIC = {"jpg", "png"};
        String[] TYPES_DOC = {"docx", "doc", "pdf", "txt"};
        String[] TYPES_VIDEO = {"avi", "mvk", "mp4"};
        String[] TYPES_AUDIO = {"mp3", "wav"};
        String[] TYPES_ARCHIVE= {"zip", "7z", "rar"};
        String[] TYPES_BINARY= {"bin", "exe", "run"};
        String[] TYPES = {TYPE_PIC, TYPE_DOC, TYPE_VIDEO,
                TYPE_AUDIO, TYPE_ARCHIVE, TYPE_BIN, TYPE_UNKNOWN};

        for(String item:TYPES)
            if(item.equals(type))
                return type;
        for(String item:TYPES_PIC)
            if(item.equals(type))
                return TYPE_PIC;
        for(String item:TYPES_DOC)
            if(item.equals(type))
                return TYPE_DOC;
        for(String item:TYPES_VIDEO)
            if(item.equals(type))
                return TYPE_VIDEO;
        for(String item:TYPES_AUDIO)
            if(item.equals(type))
                return TYPE_AUDIO;
        for(String item:TYPES_ARCHIVE)
            if(item.equals(type))
                return TYPE_ARCHIVE;
        for(String item:TYPES_BINARY)
            if(item.equals(type))
                return TYPE_BIN;
        return TYPE_UNKNOWN;
    }

}
