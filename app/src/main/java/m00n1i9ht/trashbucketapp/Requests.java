package m00n1i9ht.trashbucketapp;

import java.io.File;

import m00n1i9ht.trashbucketapp.TrashBucket.Controller;
import m00n1i9ht.trashbucketapp.TrashBucket.Server;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static m00n1i9ht.trashbucketapp.ServerData.SERVER_AVAILABLE;
import static m00n1i9ht.trashbucketapp.ServerData.SERVER_NOT_AVAILABLE;

/**
 * Created by Martynov Pavel on 18.05.2017.
 */

public class Requests {

    public static final int REQUEST_CODE_SUCCESS = 200;
    public static final int REQUEST_CODE_FAILURE = 400;
    public static final int REQUEST_CODE_VALID = 201;
    public static final int REQUEST_CODE_NOT_VALID = 202;


    public static void getToken(String serverAddress, String name, String password,
                                final ServerData serverData){
        Server server = Controller.connect(serverAddress);
        server.getToken(name, password)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        serverData.pushData(SERVER_AVAILABLE,response.body());
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        serverData.pushData(SERVER_NOT_AVAILABLE,"");
                    }
                });
    }

    public static void getListFiles(String serverAddress, String token, final ServerData serverData){
        Server server = Controller.connect(serverAddress);
        server.getListFiles(token).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                serverData.pushData(SERVER_AVAILABLE,response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });
    }

    public static void addFile(String serverAddress, String token, String absolutePath,
                               final ServerData serverData){
        File file = new File(absolutePath);
        final String fileName = file.getName();
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part fileData = MultipartBody.Part.createFormData("file", fileName,
                requestFile);

        Server server = Controller.connect(serverAddress);
        server.addFile(token, fileData).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                serverData.pushData(SERVER_AVAILABLE, fileName);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });

    }

    public static void checkValidToken(String serverAddress, String token,
                                       final ServerData serverData){
        Server server = Controller.connect(serverAddress);
        server.checkValidToken(token).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                serverData.pushData(SERVER_AVAILABLE, response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });
    }

    public static void removeFile(String serverAddress, String token, String fileId,
                                  final ServerData serverData){
        Server server = Controller.connect(serverAddress);
        server.deleteFile(token, fileId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                serverData.pushData(SERVER_AVAILABLE, response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });
    }

    public static void updateFile(String serverAddress, String token, String fileId,
                                  String absolutePath, final ServerData serverData){
        File file = new File(absolutePath);
        final String fileName = file.getName();
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part fileData = MultipartBody.Part.createFormData("file", fileName,
                requestFile);
        Server server = Controller.connect(serverAddress);
        server.updateFile(token, fileId, fileData).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                serverData.pushData(SERVER_AVAILABLE, fileName);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });

    }

    public static void getFile(String serverAddress, String token, String fileId,
                               final ServerData serverData){
        Server server = Controller.connect(serverAddress);
        server.getFile(token, fileId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                serverData.pushFile(SERVER_NOT_AVAILABLE, response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                serverData.pushData(SERVER_NOT_AVAILABLE,"");
            }
        });
    }



}
