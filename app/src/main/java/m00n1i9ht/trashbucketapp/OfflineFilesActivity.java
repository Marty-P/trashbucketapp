package m00n1i9ht.trashbucketapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.io.File;
import java.util.ArrayList;

import static m00n1i9ht.trashbucketapp.GlobalThing.PATH_APP_FILES;
import static m00n1i9ht.trashbucketapp.GlobalThing.USER_NAME;
import static m00n1i9ht.trashbucketapp.GlobalThing.isTypeMsOffice;

public class OfflineFilesActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    GlobalThing globalThing = new GlobalThing();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_files);
        getPreferences();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Offline files");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        constructList();
    }

    private void constructList(){
        ArrayList<FileData> fileData = getList();
        RecyclerView RecyclerView_listFiles = (RecyclerView) findViewById(R.id.nav_list_offline_files);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        RecyclerView_listFiles.setLayoutManager(mLayoutManager);
        FileDataRecyclerAdapter fileDataRecyclerAdapter = new FileDataRecyclerAdapter(fileData,
                new FileDataRecyclerAdapter.OnClick() {
                    @Override
                    public void onItemClicked(int position, FileDataRecyclerAdapter.ViewHolder holder,
                                              ArrayList<FileData> fileData) {
                        openFile(fileData.get(position));
                    }
                    @Override
                    public void onLongItemClicked(int position,FileDataRecyclerAdapter.ViewHolder holder,
                                                  ArrayList<FileData> fileData) {
                        showDialogFile(fileData.get(position));
                    }
                });
        RecyclerView_listFiles.setItemAnimator(new DefaultItemAnimator());
        RecyclerView_listFiles.setAdapter(fileDataRecyclerAdapter);

    }

    private ArrayList<FileData> getList(){
        ArrayList<FileData> dataFiles = new ArrayList<FileData>();
        DataBase dataBase = new DataBase(this);
        SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(DataBase.DATABASE_TABLE_FILES, null,
                DataBase.KEY_OWNER + " = ?", new String[]{globalThing.USER_NAME}, null, null, null);
        if(cursor.moveToFirst()){
            int idNAME = cursor.getColumnIndex(DataBase.KEY_NAME);
            int idIDFILE = cursor.getColumnIndex(DataBase.KEY_IDFILE);
            int idHASH = cursor.getColumnIndex(DataBase.KEY_HASH);
            int idTYPE = cursor.getColumnIndex(DataBase.KEY_TYPE);
            int idSIZE = cursor.getColumnIndex(DataBase.KEY_SIZE);
            int idDOWNTIME = cursor.getColumnIndex(DataBase.KEY_DOWNTIME);
            do{
                dataFiles.add(new FileData(cursor.getString(idNAME),
                        cursor.getString(idHASH),
                        cursor.getString(idDOWNTIME),
                        cursor.getString(idIDFILE),
                        cursor.getString(idTYPE),
                        cursor.getString(idSIZE),
                        this));
            }while(cursor.moveToNext());
        }
        cursor.close();
        dataBase.close();

        return dataFiles;
    }

    private void getPreferences(){

        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        globalThing.SERVER = sharedPreferences.getString(globalThing.KEY_SERVER, null);
        globalThing.TOKEN = sharedPreferences.getString(globalThing.KEY_TOKEN, null);
        globalThing.USER_NAME = sharedPreferences.getString(globalThing.KEY_USERNAME, null);
    }

    private void showDialogFile(final FileData fileData){
        final String[] ListOptions = {"Open", "Delete"};
        AlertDialog.Builder builder = new AlertDialog.Builder(OfflineFilesActivity.this);
        builder.setTitle("File: " + fileData.getFileName())
                .setItems(ListOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (ListOptions[which]){
                            case "Open":
                                openFile(fileData);
                                break;
                            case "Delete":
                                deleteFile(fileData);
                                constructList();
                                break;
                            case "Info":

                                break;
                        }
                    }


                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteFile(FileData fileData){
        DataBase dataBase = new DataBase(this);
        SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
        sqLiteDatabase.delete(DataBase.DATABASE_TABLE_FILES,
                DataBase.KEY_IDFILE + " = ? AND " + DataBase.KEY_OWNER + " = ?"
                , new String[]{fileData.getIdFile(), globalThing.USER_NAME} );
        dataBase.close();

        String path = getExternalFilesDir(null) + File.separator + globalThing.USER_NAME +
                File.separator + fileData.getFileName() ;
        File file = new File(path);
        file.delete();
    }

    public void openFile(FileData fileData){
        String path = getExternalFilesDir(null) + File.separator + globalThing.USER_NAME +
                File.separator + fileData.getFileName() ;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String type;
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        switch (fileData.getTitleType()){
            case "picture":
                type = "image/"+fileData.getType();
                break;
            case "document":
                type = "application/";
                if(isTypeMsOffice(fileData.getType()))
                    type += "*";
                else
                    type += fileData.getType();
                break;
            case "video":
                type = "video/"+fileData.getType();
                break;
            case "audio":
                type = "audio/"+fileData.getType();
                break;
            case "archive":
                type = "*/"+fileData.getType();
                break;
            default:
                type = "*/*";
        }


        intent.setDataAndType(uri, type);
        startActivity(intent);
    }
}
