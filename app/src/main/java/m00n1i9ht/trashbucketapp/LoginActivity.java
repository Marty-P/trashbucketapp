package m00n1i9ht.trashbucketapp;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.ResponseBody;

import static m00n1i9ht.trashbucketapp.GlobalThing.PATH_APP_FILES;
import static m00n1i9ht.trashbucketapp.Requests.REQUEST_CODE_FAILURE;
import static m00n1i9ht.trashbucketapp.Requests.REQUEST_CODE_SUCCESS;
import static m00n1i9ht.trashbucketapp.Requests.getToken;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGS";
    SharedPreferences sharedPreferences;
    GlobalThing globalThing = new GlobalThing();
    AutoCompleteTextView server,name;
    EditText password;

    private void getPreferences(){
        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        globalThing.SERVER = sharedPreferences.getString(globalThing.KEY_SERVER, null);
        globalThing.TOKEN = sharedPreferences.getString(globalThing.KEY_TOKEN, null);
        globalThing.USER_NAME = sharedPreferences.getString(globalThing.KEY_USERNAME, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        File dir = new File(PATH_APP_FILES);
        if(!dir.exists())
            dir.mkdir();

        if(globalThing.SERVER != null) {
            server = (AutoCompleteTextView) findViewById(R.id.server);
            server.setText(globalThing.SERVER);
        }

        if(globalThing.USER_NAME != null) {
            name = (AutoCompleteTextView) findViewById(R.id.name);
            name.setText(globalThing.USER_NAME);
        }

    }

    public void singIn(final View view) {

        //добавить проверку на корректность адреса
        server = (AutoCompleteTextView)findViewById(R.id.server);
        name = (AutoCompleteTextView)findViewById(R.id.name);
        password = (EditText)findViewById(R.id.password);

        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(globalThing.KEY_SERVER, server.getText().toString());
        editor.putString(globalThing.KEY_USERNAME, name.getText().toString());
        editor.commit();

        getToken(server.getText().toString(),
                name.getText().toString(),
                password.getText().toString(),
                new ServerData() {
                    @Override
                    public void pushData(int code, String response) {
                        if(code == SERVER_AVAILABLE)
                            adaptationResponse(response, view);
                        else
                            Snackbar.make(view, "Server is not available", Snackbar.LENGTH_LONG)
                               .setAction("Action", null).show();

                    }

                    @Override
                    public void pushFile(int code, ResponseBody responseBody) {

                    }
                });

    }

    private void adaptationResponse(String response, View view) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int code = jsonObject.getInt("Code");
            if(code == REQUEST_CODE_SUCCESS){
                sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES,
                        MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(globalThing.KEY_TOKEN, jsonObject.getString("Text"));
                editor.commit();

                //перезапуск приложения для авторизации
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else if(code == REQUEST_CODE_FAILURE){
                Snackbar.make(view, jsonObject.getString("Text"),
                        Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        } catch (JSONException e) {
            Log.d(TAG,"[LoginActivity] adaptationResponse: " + e.toString());
            Snackbar.make(view, "The received data are not correct",
                    Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

}

