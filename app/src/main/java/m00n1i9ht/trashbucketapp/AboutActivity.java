package m00n1i9ht.trashbucketapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    GlobalThing globalThing = new GlobalThing();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("About");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getPreferences();

        TextView textView_token = (TextView)findViewById(R.id.textView_token);
        TextView textView_server = (TextView)findViewById(R.id.textView_server);
        TextView textView_account = (TextView)findViewById(R.id.textView_account);

        textView_server.setText(globalThing.SERVER);
        textView_token.setText(globalThing.TOKEN);
        textView_account.setText(globalThing.USER_NAME);
    }

    private void getPreferences(){

        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        globalThing.SERVER = sharedPreferences.getString(globalThing.KEY_SERVER, null);
        globalThing.TOKEN = sharedPreferences.getString(globalThing.KEY_TOKEN, null);
        globalThing.USER_NAME = sharedPreferences.getString(globalThing.KEY_USERNAME, null);
    }
}
