package m00n1i9ht.trashbucketapp;


import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Martynov Pavel on 17.05.2017.
 */



public class GlobalThing {
    public static String SERVER = "";
    public static String TOKEN = "";
    public static String USER_NAME = "";
    public static String MODE_UPLOAD = "";
    public static String ARG = "";


    //Keys
    public static final String KEY_SERVER = "server";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_USERNAME = "user_name";
    public static final String NAME_PREFERENCES = "TrashBucket";
    public static final String PATH_APP_FILES = "/storage/emulated/0/TrashBucketAPP";

    //MODES UPLOAD
    public static final String MODE_UPLOAD_ADD = "ADD_FILE";
    public static final String MODE_UPLOAD_UPDATE = "UPDATE_FILE";



    public static void toastServerNotAvailable(Context context){
        Toast.makeText(context, "Server is not available", Toast.LENGTH_SHORT).show();
    }


    public static final String TYPE_PIC = "picture";
    public static final String TYPE_DOC= "document";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_AUDIO = "audio";
    public static final String TYPE_ARCHIVE = "archive";
    public static final String TYPE_BIN = "binary";
    public static final String TYPE_UNKNOWN = "unknown";


    private static final String[] TYPES_MSOFFICE = {"doc", "docx", "txt"};

    public static boolean isTypeMsOffice(String type){
        for(String item:TYPES_MSOFFICE)
            if(item.equals(type))
                return true;
        return false;
    }


}
