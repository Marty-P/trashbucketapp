package m00n1i9ht.trashbucketapp;

import okhttp3.ResponseBody;

/**
 * Created by Marty on 18.05.2017.
 */

public interface ServerData {
    public static final int SERVER_AVAILABLE = 201;
    public static final int SERVER_NOT_AVAILABLE = 500;

    void pushData(int code, String response);
    void pushFile(int code, ResponseBody responseBody);
}
