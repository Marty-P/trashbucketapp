package m00n1i9ht.trashbucketapp;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import okhttp3.ResponseBody;

/**
 * Created by Marty on 20.05.2017.
 */

public class Responses {
    public static ArrayList<FileData> toFileData(String response, Context context) throws JSONException {
        ArrayList<FileData> dataFiles = new ArrayList<FileData>();

        JSONObject jsonObject = new JSONObject(response);
        jsonObject = jsonObject.getJSONObject("Data");
        int total = jsonObject.getInt("Total");
        JSONArray list = jsonObject.getJSONArray("Items");
        for(int i = list.length()-1; i >= 0; i--){
            jsonObject = new JSONObject(list.getString(i));
            dataFiles.add(new FileData(jsonObject.getString("Name"),
                    jsonObject.getString("Hash"),
                    jsonObject.getString("UploadTime"),
                    jsonObject.getString("IdFile"),
                    jsonObject.getString("Type"),
                    jsonObject.getString("Size"),
                    context));
        }


        return dataFiles;
    }




}
