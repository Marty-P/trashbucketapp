package m00n1i9ht.trashbucketapp.TrashBucket;

import retrofit2.Retrofit;

/**
 * Created by Marty on 09.05.2017.
 */

public class Controller {
    public static Server connect(String address){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(address)
                .addConverterFactory(new ToStringConverterFactory())
                .build();
        return retrofit.create(Server.class);
    }
}