package m00n1i9ht.trashbucketapp.TrashBucket;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Created by Marty on 09.05.2017.
 */

public interface Server {
    @GET("api/gettoken")
    Call<String> getToken(@Query("name") String name, @Query("password") String password);

    @GET("api/getlistfiles")
    Call<String> getListFiles(@Query("token") String token);

    @GET("api/checkvalidtoken")
    Call<String> checkValidToken(@Query("token") String token);

    @GET("api/deletefile")
    Call<String> deleteFile(@Query("token") String token, @Query("file_id") String idFile);

    @Multipart
    @POST("api/addfile")
    Call<String> addFile(@Query("token") String token, @Part MultipartBody.Part file);

    @Multipart
    @POST("api/updatefile")
    Call<String> updateFile(@Query("token") String token, @Query("file_id") String idFile,
                            @Part MultipartBody.Part file);


    @Streaming //что то с асинхронностью делать
    @GET("api/getfile")
    Call<ResponseBody> getFile(@Query("token") String token, @Query("file_id") String idFile);
}

