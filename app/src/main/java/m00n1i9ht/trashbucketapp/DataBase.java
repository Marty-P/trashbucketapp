package m00n1i9ht.trashbucketapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Marty on 27.05.2017.
 */

public class DataBase extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LocalTrashBucket";
    public static final String DATABASE_TABLE_FILES = "LocalFiles";

    public static final String KEY_OWNER = "owner";
    public static final String KEY_IDFILE = "idFile";
    public static final String KEY_NAME = "name";
    public static final String KEY_HASH = "hash";
    public static final String KEY_TYPE = "type";
    public static final String KEY_SIZE = "size";
    public static final String KEY_DOWNTIME = "downtime";



    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DATABASE_TABLE_FILES + "("
                + KEY_OWNER + " TEXT NOT NULL, "
                + KEY_NAME + " TEXT NOT NULL, "
                + KEY_IDFILE + " TEXT NOT NULL, "
                + KEY_HASH + " TEXT NOT NULL,"
                + KEY_TYPE + " TEXT NOT NULL,"
                + KEY_SIZE + " TEXT NOT NULL,"
                + KEY_DOWNTIME + " TEXT NOT NULL)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + DATABASE_TABLE_FILES);
        onCreate(db);
    }
}
