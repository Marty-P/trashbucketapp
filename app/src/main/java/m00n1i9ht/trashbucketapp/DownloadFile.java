package m00n1i9ht.trashbucketapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;

import static m00n1i9ht.trashbucketapp.GlobalThing.PATH_APP_FILES;


/**
 * Created by Marty on 28.05.2017.
 */

public class DownloadFile extends AsyncTask <Void, Integer, Void> {
    ResponseBody responseBody;
    FileData fileData;
    FileDataRecyclerAdapter.ViewHolder holder;
    Context context;
    String userName;
    boolean statusD = false;

    DownloadFile(Context _context, ResponseBody _responseBody,
                 FileData _fileData, String _userName, FileDataRecyclerAdapter.ViewHolder _holder){
        responseBody = _responseBody;
        fileData = _fileData;
        holder = _holder;
        context = _context;
        userName = _userName;
    }
    @Override
    protected Void doInBackground(Void... params) {
        boolean writtenToDisk = writeFile(responseBody, fileData);

        if(writtenToDisk) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.KEY_OWNER, GlobalThing.USER_NAME);
            contentValues.put(DataBase.KEY_NAME, fileData.getFileName());
            contentValues.put(DataBase.KEY_IDFILE, fileData.getIdFile());
            contentValues.put(DataBase.KEY_HASH, fileData.getHash());
            contentValues.put(DataBase.KEY_TYPE, fileData.getType());
            contentValues.put(DataBase.KEY_SIZE, fileData.getSize());
            contentValues.put(DataBase.KEY_DOWNTIME, String.valueOf(System.currentTimeMillis() / 1000L));
            addDataInTable(contentValues);
            statusD = true;
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        holder.imageView_type.setVisibility(View.INVISIBLE);
        holder.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        holder.imageView_type.setVisibility(View.VISIBLE);
        holder.progressBar.setVisibility(View.INVISIBLE);
        if(statusD) {
            holder.imageView_downloaded.setVisibility(View.VISIBLE);
            Toast.makeText(context.getApplicationContext(),
                    "File \"" + fileData.getFileName() + "\" downloaded", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean writeFile(ResponseBody responseBody, FileData fileData) {
        try {
            String path = context.getExternalFilesDir(null) + File.separator +  userName;
            File dir = new File(path);
            if(!dir.exists())
                dir.mkdir();
            File file = new File(path + File.separator + fileData.getFileName());
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] buffer = new byte[4096];
                long fileSize = responseBody.contentLength();
                long fileSizeDownloaded = 0;
                inputStream = responseBody.byteStream();
                outputStream = new FileOutputStream(file);
                while (true) {
                    int read = inputStream.read(buffer);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(buffer, 0, read);
                    fileSizeDownloaded += read;
                }
                outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    private void addDataInTable(ContentValues contentValues){
        DataBase dataBase = new DataBase(context);
        SQLiteDatabase sqLiteDatabase = dataBase.getWritableDatabase();
        sqLiteDatabase.insert(DataBase.DATABASE_TABLE_FILES, null, contentValues);
    }
}
