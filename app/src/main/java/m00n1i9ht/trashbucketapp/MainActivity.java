package m00n1i9ht.trashbucketapp;


import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import ru.bartwell.exfilepicker.ExFilePicker;
import ru.bartwell.exfilepicker.data.ExFilePickerResult;

import static m00n1i9ht.trashbucketapp.GlobalThing.isTypeMsOffice;
import static m00n1i9ht.trashbucketapp.GlobalThing.toastServerNotAvailable;
import static m00n1i9ht.trashbucketapp.Requests.REQUEST_CODE_NOT_VALID;
import static m00n1i9ht.trashbucketapp.Requests.addFile;
import static m00n1i9ht.trashbucketapp.Requests.checkValidToken;
import static m00n1i9ht.trashbucketapp.Requests.getFile;
import static m00n1i9ht.trashbucketapp.Requests.removeFile;
import static m00n1i9ht.trashbucketapp.Requests.getListFiles;
import static m00n1i9ht.trashbucketapp.Requests.updateFile;
import static m00n1i9ht.trashbucketapp.Responses.toFileData;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    SharedPreferences sharedPreferences;
    GlobalThing globalThing = new GlobalThing();
    private static final int EX_FILE_PICKER_RESULT = 0;
    private static final String TAG = "LOGS";
    private SwipeRefreshLayout mSwipeRefreshLayout;


    //получение настроек
    private void getPreferences(){
        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        globalThing.SERVER = sharedPreferences.getString(globalThing.KEY_SERVER, null);
        globalThing.TOKEN = sharedPreferences.getString(globalThing.KEY_TOKEN, null);
        globalThing.USER_NAME = sharedPreferences.getString(globalThing.KEY_USERNAME, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //получение локальных данных
        getPreferences();
        if(globalThing.TOKEN == null){
            setContentView(R.layout.anonim_main);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        else{
            checkToken();
            setContentView(R.layout.activity_main);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("List Files");

            mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
            mSwipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);

            //swipe to refresh
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            refreshListFilesOnline();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    //навигационная панель
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_exit) {
            logOut();
        } else if (id == R.id.nav_list_online_files){
            refreshListFilesOnline();
        } else if (id == R.id.nav_list_offline_files){
            Intent intent = new Intent(this, OfflineFilesActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about){
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //выход
    private void logOut() {
        globalThing.TOKEN = "";
        sharedPreferences = getSharedPreferences(globalThing.NAME_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(globalThing.KEY_TOKEN, null);
        editor.commit();

        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    //переход к активити авторизации
    public void toLogInActivity(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    //обновление спискай файлов
    private void refreshListFilesOnline(){
        getListFiles(globalThing.SERVER, globalThing.TOKEN, new ServerData() {
            @Override
            public void pushData(int code, String response) {
                if(code == SERVER_AVAILABLE) {
                    constructList(response);
                }
                else
                    toastServerNotAvailable(getApplicationContext());

            }

            @Override
            public void pushFile(int code, ResponseBody responseBody) {

            }
        });
    }

    private void constructList(String response){
        try {
            ArrayList<FileData> fileData = toFileData(response, this);
            RecyclerView RecyclerView_listFiles = (RecyclerView) findViewById(R.id.list_files);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            RecyclerView_listFiles.setLayoutManager(mLayoutManager);
            FileDataRecyclerAdapter fileDataRecyclerAdapter = new FileDataRecyclerAdapter(fileData,
                    new FileDataRecyclerAdapter.OnClick() {
                @Override
                public void onItemClicked(int position, FileDataRecyclerAdapter.ViewHolder holder,
                                          ArrayList<FileData> fileData) {
                    if(fileData.get(position).isDownloded())
                        openFile(fileData.get(position));
                    else
                        downlaodFile(fileData.get(position), holder);

                }

                @Override
                public void onLongItemClicked(int position,FileDataRecyclerAdapter.ViewHolder holder,
                                              ArrayList<FileData> fileData) {
                    showDialogFile(fileData.get(position), holder);

                }
            });
            RecyclerView_listFiles.setItemAnimator(new DefaultItemAnimator());
            RecyclerView_listFiles.setAdapter(fileDataRecyclerAdapter);
        } catch (JSONException e) {
            Log.d(TAG,"refreshListFilesOnline: Can\'t convert to FileData");
            e.printStackTrace();
        }
    }

    private void showDialogFile(final FileData fileData, final FileDataRecyclerAdapter.ViewHolder holder){
        final String[] ListOptions = {"Download", "Update", "Delete"};
        if(fileData.isDownloded())
            ListOptions[0] = "Open";
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("File: " + fileData.getFileName())
                .setItems(ListOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (ListOptions[which]){
                            case "Open":
                                openFile(fileData);
                                break;
                            case "Update":
                                browserFiles(globalThing.MODE_UPLOAD_UPDATE, fileData.getIdFile());
                                break;
                            case "Download":
                                downlaodFile(fileData, holder);
                                break;
                            case "Delete":
                                deleteFile(fileData);
                                break;
                            case "Info":
                                break;
                        }
                    }


                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void deleteFile(FileData fileData){
        removeFile(globalThing.SERVER, globalThing.TOKEN, fileData.getIdFile(),
                new ServerData() {
                    @Override
                    public void pushData(int code, String response) {
                        if(code == SERVER_AVAILABLE) {
                            Toast.makeText(getApplicationContext(),
                                    "File was deleted", Toast.LENGTH_SHORT).show();
                            refreshListFilesOnline();
                        }
                        else
                            toastServerNotAvailable(getApplicationContext());

                    }

                    @Override
                    public void pushFile(int code, ResponseBody responseBody) {

                    }
                });
    }

    private void checkToken(){
        checkValidToken(globalThing.SERVER, globalThing.TOKEN, new ServerData() {
            @Override
            public void pushData(int code, String response) {
                if(code == SERVER_AVAILABLE) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if(jsonObject.getInt("Code") == REQUEST_CODE_NOT_VALID)
                            logOut();
                    } catch (JSONException e) {
                        Log.d(TAG,"[MainActivity] checkToken: " + e.toString());
                        //TODO прихерачь снекбар
                        //
                        //Snackbar.make(view, "The received data are not correct",
                        //        Snackbar.LENGTH_LONG).setAction("Action", null).show();
                    }
                }
            }

            @Override
            public void pushFile(int code, ResponseBody responseBody) {

            }
        });
    }

    public void selectFileOnFab(View view) {
        browserFiles(globalThing.MODE_UPLOAD_ADD,"");
    }

    public void browserFiles(String mode, String argument){
        globalThing.MODE_UPLOAD = mode;
        globalThing.ARG = argument;
        ExFilePicker exFilePicker = new ExFilePicker();
        exFilePicker.setCanChooseOnlyOneItem(true);
        exFilePicker.setNewFolderButtonDisabled(true);
        exFilePicker.setQuitButtonEnabled(true);
        //TODO не забыть убрать комент
        //exFilePicker.setStartDirectory("/");
        exFilePicker.setUseFirstItemAsUpEnabled(true);
        exFilePicker.setChoiceType(ExFilePicker.ChoiceType.FILES);
        exFilePicker.setSortingType(ExFilePicker.SortingType.NAME_DESC);
        exFilePicker.start(this, EX_FILE_PICKER_RESULT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EX_FILE_PICKER_RESULT) {
            ExFilePickerResult result = ExFilePickerResult.getFromIntent(data);
            if (result != null && result.getCount() > 0) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < result.getCount(); i++) {
                    stringBuilder.append(result.getNames().get(i));
                    if (i < result.getCount() - 1) stringBuilder.append(", ");
                }
                uploadFileOnServer(result.getPath() + stringBuilder.toString());
            }

        }
    }

    private void uploadFileOnServer(String absolutePath) {
        if(globalThing.MODE_UPLOAD == globalThing.MODE_UPLOAD_ADD){
            addFile(globalThing.SERVER, globalThing.TOKEN, absolutePath, new ServerData() {
                @Override
                public void pushData(int code, String response) {
                    if(code == SERVER_AVAILABLE) {
                        Toast.makeText(getApplicationContext(),
                                "File \"" + response + "\" is uploaded", Toast.LENGTH_SHORT).show();
                        refreshListFilesOnline();
                    }
                    else
                        toastServerNotAvailable(getApplicationContext());
                }

                @Override
                public void pushFile(int code, ResponseBody responseBody) {

                }
            });
        }
        else if(globalThing.MODE_UPLOAD == globalThing.MODE_UPLOAD_UPDATE){
            updateFile(globalThing.SERVER, globalThing.TOKEN, globalThing.ARG, absolutePath,
                    new ServerData(){
                @Override
                public void pushData(int code, String response) {
                    if(code == SERVER_AVAILABLE) {
                        Toast.makeText(getApplicationContext(),
                                "File \"" + response + "\" is updated", Toast.LENGTH_SHORT).show();
                        refreshListFilesOnline();
                    }
                    else
                        toastServerNotAvailable(getApplicationContext());
                }

                @Override
                public void pushFile(int code, ResponseBody responseBody) {

                }
            });
        }

    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshListFilesOnline();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 4000);
    }


    private void downlaodFile(final FileData fileData, final FileDataRecyclerAdapter.ViewHolder holder){
        getFile(GlobalThing.SERVER, GlobalThing.TOKEN, fileData.getIdFile(), new ServerData() {
            @Override
            public void pushData(int code, String response) {

            }

            @Override
            public void pushFile(int code, final ResponseBody responseBody) {
                new DownloadFile(getApplicationContext(), responseBody, fileData,
                        globalThing.USER_NAME , holder).execute();
                refreshListFilesOnline();

            }
        });

    }

    public void openFile(FileData fileData){
        String path = getExternalFilesDir(null) + File.separator + globalThing.USER_NAME +
                File.separator + fileData.getFileName() ;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String type;
        File file = new File(path);
        Uri uri = Uri.fromFile(file);
        switch (fileData.getTitleType()){
            case "picture":
                type = "image/"+fileData.getType();
                break;
            case "document":
                type = "application/";
                if(isTypeMsOffice(fileData.getType()))
                    type += "*";
                else
                    type += fileData.getType();
                break;
            case "video":
                type = "video/"+fileData.getType();
                break;
            case "audio":
                type = "audio/"+fileData.getType();
                break;
            case "archive":
                type = "*/"+fileData.getType();
                break;
            default:
                type = "*/*";
        }


        intent.setDataAndType(uri, type);
        startActivity(intent);
    }

}
